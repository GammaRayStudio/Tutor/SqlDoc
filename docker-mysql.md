Docker 安裝 MySQL 
======


下載
------
+ Docker - Get Start : <https://www.docker.com/get-started>

<br>

步驟
-------
1. 拉取 mysql 镜像
2. 檢查 mysql 镜像
3. 運行 mysql 容器
4. 進入容器
5. 登錄mysql
6. 添加遠程登錄用戶
7. SQL 操作
8. Docker 操作

<br>

拉取 mysql 镜像
------

    docker pull mysql:8

<br>

檢查 mysql 镜像
------

    docker images |grep mysql

<br>

運行 mysql 容器
------

    docker run --name sql2 -p 3306:3306 -e MYSQL_ROOT_PASSWORD=Dev127336 -d mysql:8

<br>

進入容器
------

    docker exec -it sql2 bash

<br>

登錄mysql
------

    mysql -u root -p
    ALTER USER 'root'@'localhost' IDENTIFIED BY 'Dev127336';

<br>

添加遠程登錄用戶
------

    CREATE USER 'DevAuth'@'%' IDENTIFIED WITH mysql_native_password BY 'Dev127336';
    GRANT ALL PRIVILEGES ON *.* TO 'DevAuth'@'%';

<br>

SQL 操作
------
### Show Database

    show databases;

<br>

### Create Database

    create database DevDb;

### Use Databas

    use ${dbName};

命令說明：
+ -p 3306:3306：將容器的 3306 端口映射到主機的 3306 端口。
+ -v $PWD/conf:/etc/mysql/conf.d：將主機當前目錄下的 conf/my.cnf 掛載到容器的 /etc/mysql/my.cnf。
+ -v $PWD/logs:/logs：將主機當前目錄下的 logs 目錄掛載到容器的 /logs。
+ -v $PWD/data:/var/lib/mysql ：將主機當前目錄下的data目錄掛載到容器的 /var/lib/mysql 。
+ -e MYSQL_ROOT_PASSWORD=123456：初始化 root 用戶的密碼。

<br>

Docker 操作
------
### 狀態

**容器**

    docker ps -a

+ -a : 全部狀態

**影像檔**
  
    docker images

<br>

### 啟動

    docker start container_id

<br>

### 停止

    docker stop container_id

<br>

### 刪除

    docker rm container_id

<br>

Reference
------
### Website

Docker 安装 MySQL

<http://www.runoob.com/docker/docker-install-mysql.html>

Docker Hub

<https://hub.docker.com/_/mysql>

### Docker install MySQL (Quick Guide)
```
1. docker 安装 mysql 8 版本

2.  docker 中下載 mysql
    docker pull mysql

3. 啓動
    docker run --name mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=Dev127336 -d mysql:8

4. 進入容器
    docker exec -it mysql bash

5. 登錄mysql
    mysql -u root -p
    ALTER USER 'root'@'localhost' IDENTIFIED BY 'Dev127336';

6. 添加遠程登錄用戶
    CREATE USER 'DevAuth'@'%' IDENTIFIED WITH mysql_native_password BY 'Dev127336';
    GRANT ALL PRIVILEGES ON *.* TO 'DevAuth'@'%';
```


