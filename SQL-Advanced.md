進階的 SQL 語法
======
`DBA , Database Administrator`

Contents
------
+ Views
+ Having
+ Temporary Tables

<br>

DEMO TABLE
------
### Create
```sql
create table app_info (
	id int auto_increment primary key ,
	name	nvarchar(50),
	version	nvarchar(30),
	author	nvarchar(50),
	date	datetime,
	remark	nvarchar(100)
);
```
### Data
```sql
insert into app_info(name,version,author,date,remark) values
('JavaProjSE-v1.0.3','1.0.3','Enoxs','2019-07-24' ,'Java Project Simple Example - Version 1.0.3'),
('JUnitSE','1.0.2','Enoxs','2019-08-17' ,'Java Unit Test Simple Example'),
('SpringMVC-SE','1.0.2','Enoxs','2019-07-31' ,'Java Web Application Spring MVC');
```

<br>

Views `視圖`
------
### Create View
```sql
create view show_app as
select id,name,version
from app_info;
```

### Select View
```sql
select * from show_app;
```
### Insert View
```sql
insert into show_app(id, name, version) values(0, 'AppNew', 'v1.0.1');
```

+ 必須包含所有 not null 欄位，否則無法生效

### Update View
```sql
update show_app set name = 'AppNew' , version = '1.0.2' where id = 2;
```

**創建 View 的 Select 語法限制** 
+ select 子句
  + 不包含 關鍵字 distinct
  + 不包含 order by 子句
  + 不包含 匯總函數
  + 不包含 集合函數 `set`
  + 不包含 集合運算符 `set`
+ from 子句 
  + 不包含 多個表
+ where 子句
  + 不包含 子查詢
+ 不包含 group by
+ 不包含 having
+ 計算類的欄位，可能無法更新

### Delete View
```sql
delete from show_app where id = 3;
```

### Drop View
```sql
drop view show_app;
```

<br>

### With Check Option
```sql
create view show_app_chk as
select id,name,version
from app_info
where version is not null
with check option;
``` 

+ with check option : 可以確保 update 與 insert 滿足 View 定義的條件

**update**
```sql
update show_app set name = 'AppNew' , version = "1.0.2" where id = 2;
update show_app_chk set name = 'AppNew' , version = NULL where id = 2;
```

<br>

Having 子句
------
`指定過濾的搜尋結果條件`

### 語法
```sql
SELECT  column1 , column2
FROM table1 , table2
WHERE condition
GROUP BY column1 , column2
HAVING condition
ORDER BY column1 , column2
```

### SQL
```sql
select version , count(*) from app_info group by version order by version asc;
select version , count(*) from app_info where version = '1.0.2' group by version order by version asc;

select version , count(*) from app_info group by version
having count(*) > 3 and version = '1.0.2'
order by version asc ;
```

<br>

Temporary Table
------
`暫存資料表`

**create**
```sql
create temporary table app_info_tmp (
    id int auto_increment primary key ,
    name    nvarchar(50),
    version    nvarchar(30),
    author    nvarchar(50),
    date    datetime,
    remark    nvarchar(100)
);
```

**data**
```sql
insert into app_info_tmp(name,version,author,date,remark) values
('JavaProjSE-v1.0.3','1.0.3','Enoxs','2019-07-24' ,'Java Project Simple Example - Version 1.0.3'),
('JUnitSE','1.0.2','Enoxs','2019-08-17' ,'Java Unit Test Simple Example'),
('SpringMVC-SE','1.0.2','Enoxs','2019-07-31' ,'Java Web Application Spring MVC');
```

**query**
```sql
select * from app_info_tmp;
```

**drop**
```sql
drop table app_info_tmp;
```

<br>

Reference
------
### MySQL 交易功能 Transaction 整理
<https://xyz.cinc.biz/2013/05/mysql-transaction.html>



