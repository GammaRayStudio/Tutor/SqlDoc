SQL 入門指南
======
`Structured Query Language , 結構化查詢語言`

SQL 概述
------
### 用途

    用於存儲、操作和檢索，保存在「關聯式資料庫」的數據


**資料庫的標準語言**
```
所有關係數據庫管理系統 (RDMS)，如: 
MySQL、MS Access、Oracle、Sybase、Informix、Postgres 和 SQL Server，
都使用 SQL 作為其標準數據庫語言。
```



<br>

### 關聯式資料庫

+ MySQL
+ MS-SQL (SQL Server) `T-SQL`
+ Oracle `PL/SQL`
+ Postgres

<br>

### SQL 簡史
+ 1970 年 - IBM 的 Edgar F. "Ted" Codd 博士被稱為關聯式資料庫之父。他描述了數據庫的關係模型。
+ 1974 年 - 結構化查詢語言出現。
+ 1978 年 - IBM 致力於開發 Codd 的想法並發布了名為 System/R 的產品。
+ 1986 年 - IBM 開發了關係數據庫的第一個原型並由 ANSI 標準化。
  + 第一個關係數據庫是由 Relational Software 發布的，後來被稱為 Oracle。

<br>

SQL 命令
------
### DDL - Data Definition Language
`數據定義語言`

+ CREATE - 在資料庫創建表(Table)、視圖(View)、或其他物件 
+ ALTER - 修改資料庫中的物件，例如 : 表(Table)
+ DROP - 在資料庫刪除表(Table)、視圖(View)、或其他物件 

<br>

### DML - Data Manipulation Language
`數據操作語言`

+ SELECT - 檢索資料
+ INSERT - 新增資料
+ UPDATE - 修改資料
+ DELETE - 刪除資料

**常見的CRUL**

  所有的系統核心功能，新增、讀取、變更、刪除
  (Create / Read / Update / Delete)

+ [Wiki : 增刪改查](https://zh.wikipedia.org/wiki/%E5%A2%9E%E5%88%AA%E6%9F%A5%E6%94%B9)


<br>

### DCL - Data Control Language
`數據控制語言`

+ GRANT - 授予權限
+ REVOK - 撤銷權限


<br>

### SQL 快速語法
**DDL**
Database :
```sql
create database DevDb;
show databases
use DevDb;
```


Table :
```sql
drop table if exists app_info;
create table app_info (
	id int auto_increment primary key ,
	name	nvarchar(50),
	version	nvarchar(30),
	author	nvarchar(50),
	date	datetime,
	remark	nvarchar(100)
);
```

**DML**

insert : 
```sql
insert into app_info(name,version,author,date,remark) values
('JavaProjSE-v1.0.3','1.0.3','Enoxs','2019-07-24' ,'Java Project Simple Example - Version 1.0.3'),
('JUnitSE','1.0.2','Enoxs','2019-08-17' ,'Java Unit Test Simple Example'),
('SpringMVC-SE','1.0.2','Enoxs','2019-07-31' ,'Java Web Application Spring MVC 框架 - Simple Example 未整合持久化框架（MyBatis）');
```

select :
```
select * from app_info;
```

+ DBeaver - Generate SQL

<br>

RDBMS - 關聯式資料庫
------
`RDBMS , Relational Database Management System`

    現實世界的資料通常都會互有關聯

<br>

### 表(Table)
`數據的集合，許多行與列組成`

| 主鍵 | 專案名稱 | 專案版本 | 專案作者 | 發佈時間 | 專案描述 | 
| --- | --- | --- | --- | --- | --- |
| id | name | version | author | date | remark |
| 1 | JavaProjSE | 1.0.3 | Enoxs | 2019-07-24 | Java Project Simple Example | 
| 2 | JUnitSE | 1.0.2 | Enoxs | 2019-08-17 | Java Unit Test Simple Example | 
| 3 | SpringMVC-SE | 1.0.2 | Enoxs | 2019-07-31 | Java Web Application Spring |

<br>

### 欄(Field)

| 主鍵 | 專案名稱 | 專案版本 | 專案作者 | 發佈時間 | 專案描述 | 
| --- | --- | --- | --- | --- | --- |
| id | name | version | author | date | remark |

<br>

### 行(Row)

| 主鍵 | 專案名稱 | 專案版本 | 專案作者 | 發佈時間 | 專案描述 | 
| --- | --- | --- | --- | --- | --- |
| id | name | version | author | date | remark |
| 1 | JavaProjSE | 1.0.3 | Enoxs | 2019-07-24 | Java Project Simple Example | 

    表的主要紀錄單位，以表一的範例而言就是三筆資料。

<br>

### 列(Column)

| 專案名稱 |
| --- |
| name |
| JavaProjSE |
| JUnitSE |
| SpringMVC-SE |

    特定欄位相關資料

**實務經驗**
+ create_user : 建立資料人員
+ create_time : 建立資料時間
+ modify_user : 修改資料人員
+ modify_time : 修改資料時間

**NULL 值**

   代表沒有值的資料，預設的留空值。

**欄位規則(約束)**
+ NOT NULL : 不可以為 NULL 值 
+ DEFAULT : 沒有指定時，預設資料
+ UNIQUE : 唯一值
+ PRIMARY KEY : 主鍵，識別表紀錄的差異
+ FOREIGN KEY : 外鍵，識別表與表之間的鍵值
+ CHECK : 檢查條件，符合才能新增
+ INDEX : 索引值，加快檢索速度

```
create table app_info (
	id int auto_increment primary key ,
	name	nvarchar(50) not null,
	version	nvarchar(30) default '1.0.1' , 
	author	nvarchar(50) ,
	date	datetime,
	remark	nvarchar(100) null
);
```

<br>

[資料庫正規化]
------
`標準化資料格式`

+ [第一正規化 (1NF , Normal Form)](https://zh.wikipedia.org/wiki/%E7%AC%AC%E4%B8%80%E6%AD%A3%E8%A6%8F%E5%8C%96)
+ [第二正規化 (2NF , Normal Form)](https://zh.wikipedia.org/wiki/%E7%AC%AC%E4%BA%8C%E6%AD%A3%E8%A6%8F%E5%8C%96)
+ [第三正規化 (3NF , Normal Form)](https://zh.wikipedia.org/wiki/%E7%AC%AC%E4%B8%89%E6%AD%A3%E8%A6%8F%E5%8C%96)

[資料庫正規化]:https://zh.wikipedia.org/wiki/%E6%95%B0%E6%8D%AE%E5%BA%93%E8%A7%84%E8%8C%83%E5%8C%96

<br>

### 為什麼 ?

+ 消除冗餘數據
  + 例如: 將相同的數據重複紀錄在多個表中。
+ 確保數據依賴是有意義的。
  + 讓資料表的都是不可分割的欄位組成

<br>

### 第一正規化 (1NF , Normal Form)
`定義一張表的內容，欄位越相關越好`

**關注點**
+ 有主鍵 (唯一識別碼)
+ 不重複 (欄位單一值)

<br>

**範例(未正規化)**

程式專案資訊 : app_info

| 專案名稱 | 專案版本 | 專案作者 | 發佈時間 | 專案描述 | 
| --- | --- | --- | --- | --- |
| name | version | author | date | remark |
| JavaProjSE | 1.0.1,1.0.2 | Enoxs | 2019-07-24,2019-08-25 | Java Project Simple Example | 
| JUnitSE | 1.0.1,1.0.2 | Enoxs | 2019-08-17,2019-09-30 | Java Unit Test Simple Example | 
| SpringMVC-SE | 1.0.1 | Enoxs | 2019-07-31 | Java Web Application Spring |


**範例(正規化)**

程式專案資訊 : app_info

| id | name | version | author | date | remark |
| --- | --- | --- | --- | --- | --- |
| 1 | JavaProjSE | 1.0.1 | Enoxs | 2019-07-24 | Java Project Simple Example | 
| 2 | JUnitSE | 1.0.1 | Enoxs | 2019-08-17 | Java Unit Test Simple Example | 
| 3 | SpringMVC-SE | 1.0.1 | Enoxs | 2019-07-31 | Java Web Application Spring |
| 4 | JavaProjSE | 1.0.2 | Enoxs | 2019-08-25 | Java Project Simple Example | 
| 5 | JUnitSE | 1.0.2 | Enoxs | 2019-09-30 | Java Unit Test Simple Example | 

+ Key 值 : 為了區分必須要紀錄兩筆一模一樣的資料時

<br>

### 第二正規化 (2NF , Normal Form)

**關注點**

+ 符合第一正規化
+ 消除部分相依
  + 欄位與主鍵要有完全依賴關係，如果只有部分相關，必須獨立出來

```
分割部分相依的欄位
```

<br>

**範例(未正規化)**

程式專案資訊 : app_info

| id | name | version | author | date | remark |
| --- | --- | --- | --- | --- | --- |
| 1 | JavaProjSE | 1.0.1 | Enoxs | 2019-07-24 | Java Project Simple Example | 
| 2 | JUnitSE | 1.0.1 | Enoxs | 2019-08-17 | Java Unit Test Simple Example | 
| 3 | SpringMVC-SE | 1.0.1 | Enoxs | 2019-07-31 | Java Web Application Spring |
| 4 | JavaProjSE | 1.0.2 | Enoxs | 2019-08-25 | Java Project Simple Example | 
| 5 | JUnitSE | 1.0.2 | Enoxs | 2019-09-30 | Java Unit Test Simple Example | 


**範例(正規化)**

程式專案資訊 : app_info

| 主鍵 | 專案名稱 | 專案作者 | 專案描述 | 
| --- | --- | --- | --- |
| id | name | author | remark |
| 1 | JavaProjSE | Enoxs | Java Project Simple Example | 
| 2 | JUnitSE | Enoxs | Java Unit Test Simple Example | 
| 3 | SpringMVC-SE | Enoxs | Java Web Application Spring |

程式版本資訊 : version_info

| 主鍵 | 程式 ID | 版本名稱 | 發佈時間 | 
| --- | --- | --- | --- |
| id | app_id | version_name | release_date |
| 1 | 1 | 1.0.1 | 2019-07-24 | 
| 2 | 2 | 1.0.1 | 2019-08-17 | 
| 3 | 3 | 1.0.1 | 2019-07-31 | 
| 4 | 1 | 1.0.2 | 2019-08-25 | 
| 5 | 2 | 1.0.2 | 2019-09-30 | 

<br>

### 第三正規化 (3NF , Normal Form)

**關注點**
+ 符合第二正規化
+ 消除遞移相依
  + 欄位 A 和主鍵相關，欄位 B 和欄位 A 相關，欄位 A 和主鍵就是地移相依

**範例(未正規化)**

程式專案資訊 : app_info


| 主鍵 | 專案名稱 | 專案作者 | 專案描述 | 群組 ID | 群組名稱 |
| --- | --- | --- | --- | --- | --- |
| id | name | author | remark | group_id | group_name |
| 1 | JavaProjSE | Enoxs | Java Project Simple Example | 1 | Java 程式範例
| 2 | JUnitSE | Enoxs | Java Unit Test Simple Example | 2 | Java 框架範例
| 3 | SpringMVC-SE | Enoxs | Java Web Application Spring | 2 | Java 框架範例

**範例(正規化)**

程式專案資訊 : app_info

| 主鍵 | 專案名稱 | 專案作者 | 專案描述 | 
| --- | --- | --- | --- |
| id | name | author | remark |
| 1 | JavaProjSE | Enoxs | Java Project Simple Example | 
| 2 | JUnitSE | Enoxs | Java Unit Test Simple Example | 
| 3 | SpringMVC-SE | Enoxs | Java Web Application Spring |

程式群組資訊 : app_group

| 主鍵 | 群組名稱 | 群組描述 | 
| --- | --- | --- |
| 1 | Java 程式範例 | JavaSE 語法範例 |
| 2 | Java 框架範例 | Java 框架範例 |

群組專案對映 : app_group_project

| 主鍵 | 群組 ID | 專案 ID | 
| --- | --- | --- |
| 1 | 1 | 1 |
| 2 | 2 | 2 |
| 3 | 2 | 3 |

<br>

**個人分類**
+ 第一正規化 : only one
+ 第二正規化 : one to many
+ 第三正規化 : many to many

<br>

SQL 語法速查表
------
+ [SQL - Synax](SQL-Synax.md)


<br>

SQL 數據類型 
------
`data_type`

### 整數

| 型態 | 範圍 |
| --- | --- |
| bigint | -9,223,372,036,854,775,808	~ 9,223,372,036,854,775,807 |
| int	| -2,147,483,648 ~ 2,147,483,647 |
| smallint | -32,768 ~ 32,767 |
| tinyint	| 0	~ 255 |
| bit	| 0	~ 1 |
| decimal	| -10^38 +1	~ 10^38 -1 |
| numeric	| -10^38 +1	~ 10^38 -1 |
| money	| -922,337,203,685,477.5808	~ +922,337,203,685,477.5807 |
| smallmoney | -214,748.3648 ~ +214,748.3647 |

+ int `數字`
+ decimal `金額`

<br>

### 浮點數

| 型態 | 範圍 |
| --- | --- |
| float |	-1.79E + 308 ~ 1.79E + 308 |
| real | -3.40E + 38 ~ 3.40E + 38 |

<br>

### 時間

| 型態 | 範圍 |
| --- | --- |
| datetime | Jan 1, 1753 ~ Dec 31, 9999 |
| smalldatetime |	Jan 1, 1900 ~ Jun 6, 2079 |
| date |	ex: June 30, 1991 |
| time |	ex: 12:3 P.M. |

<br>

### 字串

**一般字串**

| 型態 | 範圍 |
| --- | --- |
| char | Max 8,000 字符 (固定長度) |
| varchar | Max 8,000 字符 (可變長度) |
| varchar(max) | Max 2E+31 字符 (可變長度 / SQL Server) |
| text | Max 2,147,483,647 字符 (可變長度) |

**Unicode字串**

| 型態 | 範圍 |
| --- | --- |
| nchar | Max 4,000 字符 (固定長度) 
| nvarchr | Max 4,000 字符 (可變長度) 
| nvarchar(max) | Max 4,000 字符 (可變長度 / SQL Server) 
| ntext | Max 1,073,741,823 字符 (可變長度)

+ var : 長度可否變動，需多餘空間紀錄地址 (2 Byte)
+ n : 萬國碼支援，資料內有中文時使用 (2Byte)
+ 效能 : (nvarchar, varchar) < (nchar, char)
+ 空間 : (nvarchar, nchar) >> (varchar, char)
  + char(n)：n Byte
  + varchar(n)：(n + 2) Byte  --2Byte記錄地址
  + nchar(n)：(2 * n) Byte
  + nvarchar(n)：(2 * n + 2) Byte

**建議**
+ 確認一定長度，且只會有英數字：char
+ 確認一定長度，且可能會用非英數以外的字元：nchar
+ 長度可變動，且只會有英數字：varchar
+ 長度可變動，且可能會用非英數以外的字元：nva

<br>

### 二進制

| 型態 | 範圍 |
| --- | --- |
| binary | Max 8,000 bytes (固定長度) |
| varbinary | Max 8,000 byte (可變長度) |
| varbinary(max) | Max 2E + 31 byte (可變長度 / SQL Server) |
| image | Max 2,147,483,647 bytes (可變長度)

<br>

### 雜項

| 型態 | 描述 |
| --- | --- |
| sql_variant | 任意值，除了 text ,ntext, timestamp (SQL Server) |
| timestamp | 時間戳，資料庫唯一編碼 |
| uniqueidentifier | 全局唯一識別碼 (GUID , globally unique identifier) |
| xml | XML 資料 (SQL Server) |
| cursor | 參考指標對象 |
| table | 資料表結果，供後續處理 |


<br>


SQL 運算符
------
### 算術運算符
+ `+`
+ `-`
+ `*`
+ `/`
+ `%`

### 比較運算符
+ `=`
+ `!=` / `<>`
+ `>`
+ `<`
+ `>=`
+ `<=`
+ `!<`
+ `!>`

### 邏輯運算符
+ ALL
+ AND
+ ANY
+ BETWEEN
+ EXIST
+ IN
+ LIKE
+ NOT
+ OR
+ IS NULL
+ UNIQUE

<br>

基本 SQL 語法
------
`SQL Syntax`

### DDL
**CREATE**
```sql
create table app_info (
	id int auto_increment primary key ,
	name	nvarchar(50),
	version	nvarchar(30),
	author	nvarchar(50),
	date	datetime,
	remark	nvarchar(100)
);

-- Generate SQL

CREATE TABLE `app_info` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `version` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb3;
```

**ALTER**
```sql
新增欄位 -- ADD
alter table app_info add new_column nvarchar(64) null;

修改欄位 -- CHANGE
alter table app_info change new_column description nvarchar(100) null;

刪除欄位 - DROP
alter table app_info drop column description

```

**DROP**
```sql
drop table if exists app_info;
```

### DML
+ SELECT
+ INSERT
+ UPDATE
+ DELETE

**Select**
```sql
select * from app_info;

-- Generate SQL

SELECT id, name, version, author, `date`, remark
FROM DevDb.app_info;

```

Expressions
```
SELECT column1, column2, columnN 
FROM table_name 
WHERE [CONDITION|EXPRESSION];
```

Where Condition :
```sql
-- Select By
select id,name,version from app_info where version='1.0.3';

-- Select By Condition 01
select * from app_info where id > 1; -- > < =

-- Select By Condition 02
select * from app_info where version != '1.0.2'; -- MS-SQL : NOT

-- Select By AND
select * from app_info where version='1.0.3' and author = 'Enoxs';

-- Select By OR
select * from app_info where version='1.0.3' or author = 'Enoxs';
```

**INSERT**
```sql
insert into app_info(name,version,author,date,remark) values
('JavaProjSE-v1.0.3','1.0.3','Enoxs','2019-07-24' ,'Java Project Simple Example - Version 1.0.3')

-- Generate SQL
INSERT INTO DevDb.app_info
(name, version, author, `date`, remark)
VALUES('', '', '', '', '');
```

**UPDATE**
```sql
update app_info 
set name='MyBatisSE', version='1.0.1', author='Enoxs', `date`='2021-08-06', remark='MyBatis - Sample Example' 
where id=0 ;

-- Update By
update app_info
set name='MyBatisSE', version='1.0.2', author='Enoxs', `date`='2021-08-06', remark='MyBatis - Sample Example'
where name='MyBatisSE' and version = '1.0.1';

-- Generate SQL
UPDATE DevDb.app_info
SET name='', version='', author='', `date`='', remark=''
WHERE id=0;
```

**DELETE**
```sql
delete from app_info where id=0;

-- Generate SQL
DELETE FROM DevDb.app_info
WHERE id=0;
```

**Where Clause**
```sql
-- Select By Like 01 
select * from app_info where name like '%SE';

-- Select By Like 02 
select * from app_info where name like '%SE-%';

-- Select By Like 03 
select * from app_info where name like 'J%';

-- Select By Like 04 
select * from app_info where version like '1.0._';

-- Select By Like 05 
select * from app_info where version like '_.0.2';

-- Select By Limit 
select * from app_info limit 2; -- MS-SQL : TOP 2
-- MS-SQL : select top 2 * from app_info;

-- Select By Order 01 
select * from app_info order by id asc;

-- Select By Order 02 
select * from app_info order by id desc;

-- Select By Order 03 
select * from app_info order by version , id asc;

-- Select By Group 
select version , count(*) from app_info group by version order by version asc;
-- 搭配聚合函式一起使用 : AVG , COUNT , MAX , MIN , SUM
-- 順序 where -> group by -> order by

-- Select By Distinct 
select distinct author , version from app_info;

-- Select By In 
select * from app_info where id in (1,2,3);

-- Select By Between
select * from app_info where id BETWEEN 1 and 3;

-- Select Count(*)
select count(*) from app_info;
```

<br>

進階 SQL 語法
------
`後端工程師，進階語法`

+ Constraints
+ Joins `*`
+ Unions
+ Alias
+ Sub Query
+ Transaction
+ Truncate Table

<br>

### 表格約束規則
`Constraints`

+ `NOT NULL` :  不能有 NULL 值 `DEV`
+ `DEFAULT` : 沒有指定時，預設值 `DEV`
+ `UNIQUE` : 限制列中所有的值都不相同
+ `PRIMARY KEY` : 指定欄位為表的唯一值 `DEV`
+ `FOREIGN KEY` : 指定欄位為表的外鍵 `DEV`
+ `CHECK` : 檢查欄位的值，滿足特定條件
+ `INDEX` : 為表格欄位建立索引，提升數據檢索速度 `DEV`

<br>

**NOT NULL**

```sql
create table demo_not_null(
	id int not null, 
  name varchar(20) not null
);

create table demo_null(
  id int not null,
  name varchar(20)
);

insert into demo_null (id, name) values ('1' , 'demo');
insert into demo_null (id, name) values ('2' , null );

```

alter:
```sql
alter table demo_not_null modify name varchar(20) null;
alter table demo_not_null modify name varchar(20) not null;
```

select:
```sql
select * from demo_null;
select * from demo_null where name is null;
select * from demo_null where name is not null;
```

<br>

**DEFAULT**
```sql
create table demo_default(
	id int not null,
	name varchar(20) default 'No Name'
);
```

alter:
```sql
alter table demo_default modify name varchar(20) default 'DevAuth';
alter table demo_default alter column name drop default;
```

insert:
```sql
insert into demo_default (id) values (1);
select * from demo_default;
```

<br>

**UNIQUE**
```sql
create table demo_unique(
	id int not null,
	name varchar(20) not null unique
);
```

alter:
```sql
alter table demo_unique modify id int not null unique;
alter table demo_unique add constraint myuniqueconstraint unique(id, name);
alter table demo_unique drop constraint myuniqueconstraint;
alter table demo_unique drop index myuniqueconstraint;
```

<br>

**PRIMARY KEY**
```sql
create table demo_primary_01(
    id int not null,
    name varchar(20) not null,
    primary key (id)
);

create table demo_primary_02(
    id int not null,
    name varchar(20) not null,
    primary key (id,name)
);
```

alter:
```sql
create table demo_primary(
    id int not null,
    name varchar(20) not null
);

alter table demo_primary add primary key(id);
alter table demo_primary add constraint pk_custid primary key (id, name);
alter table demo_primary drop primary key;
```

create:
```sql
create table app_info (
    id int auto_increment primary key ,
    name    nvarchar(50),
    version    nvarchar(30),
    author    nvarchar(50),
    date    datetime,
    remark    nvarchar(100)
);
```

data:
```sql
insert into app_info (name, version, author, date, remark)
values('App', '1.0.1', 'DevAuth', '2021-01-01 12:00:00', 'App-v1.0.1');
```

<br>

**FOREIGN KEY**
```sql
create table demo_table(
	id int not null,
	name varchar(20) not null,
	primary key(id)
);
create table demo_sub_table(
	id int not null,
	demo_id int not null,
	name varchar(20) not null , 
  primary key(id) , 
  foreign key(demo_id) references demo_table(id)
);
```
alter:
```sql
alter table demo_sub_table add foreign key (demo_id) references demo_table(id);
alter table demo_sub_table drop foreign key demo_id; -- error
alter table demo_sub_table drop foreign key demo_sub_table_ibfk_1; -- MySQL 版本
```

<br>

**CHECK**
```sql
create table demo_check(
    id int not null ,
    name varchar(20) not null,
    numbers int not null check (numbers > 0)
);
```

```sql
alter table demo_check modify numbers int not null check (number > 5); -- MySQL 無作用 , MS-SQL 無法執行
alter table demo_check add constraint mycheckconstraint check (numbers > 5);
alter table demo_check drop constraint mycheckconstraint;
```

+ Modify Check Constraints - Mircosoft
  + <https://docs.microsoft.com/en-us/sql/relational-databases/tables/modify-check-constraints?view=sql-server-ver16>

<br>

**INDEX**

DDL:
```sql
create table demo_index(
    id int not null ,
    name varchar(20) not null,
    primary key(id)
);
```

<br>

Synax
```sql
create index index_name on table name
```

單列索引
```sql
create index idx_name on demo_index (name); 
```

綜合索引
```sql
create index idx_name on demo_index (id , name);
```

alter:

```sql
alter table demo_index drop index idx_name;
```

注意使用時機:
+ 數據量較小的表不使用
+ 頻繁與大批量更新或新增數據的表不使用
+ 會出現 NULL 的欄位不使用
+ 會頻繁操作的欄位不使用


<br>

SQL - JOIN `*`
------
`合併兩張表的多筆數據`

**DDL**
```sql
create table demo_main(
    id int not null,
    name varchar(20) not null,
    primary key(id)
);

create table demo_join(
    id int not null,
    main_id int not null,
    var_int int default 100,
    var_str varchar(50) default 'text'
);

INSERT INTO demo_main (id, name) VALUES(1, 'demo01');
INSERT INTO demo_main (id, name) VALUES(2, 'demo02');
INSERT INTO demo_main (id, name) VALUES(3, 'demo03');
INSERT INTO demo_main (id, name) VALUES(4, 'demo04');
INSERT INTO demo_main (id, name) VALUES(5, 'demo05');

INSERT INTO demo_join (id, main_id, var_int, var_str) VALUES(1, 1, 1 , 'text - 01');
INSERT INTO demo_join (id, main_id, var_int, var_str) VALUES(2, 2, 2 , 'text - 02');
INSERT INTO demo_join (id, main_id, var_int, var_str) VALUES(3, 3, 3 , 'text - 03');
INSERT INTO demo_join (id, main_id, var_int, var_str) VALUES(4, 6, 4 , 'text - 06');
INSERT INTO demo_join (id, main_id, var_int, var_str) VALUES(5, 7, 5 , 'text - 07');
INSERT INTO demo_join (id, main_id, var_int, var_str) VALUES(6, 8, 6 , 'text - 08');
```

**SELECT**
```sql
select * from demo_main;
select * from demo_join;

select name , var_int , var_str from demo_main,demo_join where demo_main.id = demo_join.main_id;
```

demo_main:

| id | name |
| --- | --- | 
| 1 | demo01 |
| 2 | demo02 | 
| 3 | demo03 |
| 4 | demo04 |
| 5 | demo05 |


demo_join:

| id | main_id | var_int | var_str |
| --- | --- | --- | --- |
| 1 | 1 | 1 | text - 01 |
| 2	| 2 | 2 | text - 02 |
| 3 | 3 | 3 | text - 03 |
| 4	| 6 | 4	| text - 06 |
| 5	| 7 | 5 | text - 07 |
| 6	| 8 | 6 | text - 08 |

join_table:

| name | var_int | var_str |
| --- | --- | --- |
| demo01 | 1 | text - 01 |
| demo02 | 2 | text - 02 |
| demo03 | 3 | text - 03 |

<br>

### 不同類型的連接
+ INNER JOIN : 回傳匹配兩張資料表的紀錄 `交集`
+ LEFT JOIN : 回傳左邊表格匹配的紀錄，右邊不用匹配 `以左邊為基準`
+ RIGHT JOIN : 回傳右邊表格匹配的紀錄，左邊不用要匹配 `以右邊為基準`
+ FULL JOIN : 回傳任一表格匹配的紀錄 `相似於聯集，中間重複`
+ SELF JOIN : 將自身的表格，當成額外的表格連接 `特殊用途`
+ CARTESIAN JOIN : 回傳兩個或多個表格記錄集的「笛卡爾積」 `交叉連接 或 笛卡爾連接`

<br>

**INNER JOIN**
```sql
select name , var_int , var_str from demo_main inner join demo_join
on demo_main.id = demo_join.main_id;
```

<br>

**LEFT JOIN**
```sql
select name , var_int , var_str from demo_main left join demo_join
on demo_main.id = demo_join.main_id;
```

<br>

**RIGHT JOIN**
```sql
select name , var_int , var_str from demo_main right join demo_join
on demo_main.id = demo_join.main_id;
```

<br>

**FULL JOIN**
```sql
select demo_data.var_no , var_int , var_str from demo_data right join demo_join 
on demo_data.var_no = demo_join.var_no; -- MySQL 不支援 FULL JOIN

-- 使用 UNION ALL 方法代替
select name , var_int , var_str from demo_main left join demo_join
on demo_main.id = demo_join.main_id
union all
select name , var_int , var_str from demo_main right join demo_join
on demo_main.id = demo_join.main_id;
```

<br>

**SELF JOIN**
```sql
select a.id , a.name , b.id , b.name from demo_main a , demo_main b
where a.id < b.id;
```

<br>

**CARTESIAN JOIN**
```sql
-- 5 x 6 
select demo_main.id , demo_join.var_str  from demo_main , demo_join; 
```

<br>

UNIONS 子句
------
### UNION FULL
`FULL JOIN`

```sql
select name , var_int , var_str from demo_main left join demo_join
on demo_main.id = demo_join.main_id
union all
select name , var_int , var_str from demo_main right join demo_join
on demo_main.id = demo_join.main_id;
```

### UNION
```sql
select name , var_int , var_str from demo_main left join demo_join
on demo_main.id = demo_join.main_id
union
select name , var_int , var_str from demo_main right join demo_join
on demo_main.id = demo_join.main_id;
```

<br>

Alias 語法
------
`別名 , 縮寫 AS`

**create**
```sql
create table demo_alias(
    id int not null,
    name varchar(20) not null,
    primary key(id)
);
```

**data**
```sql
insert into demo_alias (id, name) values(1, 'demo');
```

<br>

### table alias
```sql
select da.id , da.name from demo_alias da ;
```

### column alias
```sql
select id as demo_id , name as demo_name from demo_alias ;
```

<br>

### join table

**ddl**
```sql
create table demo_main(
    id int not null,
    name varchar(20) not null,
    primary key(id)
);

create table demo_join(
    id int not null,
    main_id int not null,
    var_int int default 100,
    var_str varchar(50) default 'text'
);
```

**select**
```sql
select d1.id as main_id , d2.id as join_id
from demo_main as d1
inner join demo_join as d2
on d1.id = d2.main_id;
```

<br>

Sub Query
------
`子查詢`

### Select 語法
```sql
select * from app_info
where id in
(
  select id from app_info where version = '1.0.2'
)
```

### Insert 語法
`克隆表格`
```sql
insert into app_info (name , version)
select name , version from app_info
where id in
(
  select id from app_info where version = '1.0.2'
);
```

### Update 語法
```sql
update app_info app , (
	select id , name , version
	from app_info where id = 1
) info
set app.name = info.name , app.version = info.version
where app.id = 2;

update app_info app
join app_info info on info.id = 1
set app.name = info.name , app.version = info.version
where app.id = 2;
```

### Delete 語法
```sql
delete from app_info
where id in
(
	select id from app_info 
	where version = '1.0.3'
); -- MySQL 有問題， MS-SQL、ORACLE 可以這樣寫

delete from app_info
where id in
(
	select info.id from 
	(
		select id from app_info 
		where version = '1.0.3'
	)as info 
);

```

<br>

Transactions `交易`
------
### 交易功能 : 四種特性
`ACID`

**Atomicity**`(原子性、不可分割)`

```
交易內的 SQL 指令，不管在任何情況，都只能是全部執行完成，或全部不執行。
若是發生無法全部執行完成的狀況，則會回滾(rollback)到完全沒執行時的狀態。
```

**Consistency**`(一致性)`

```
交易完成後，必須維持資料的完整性。
所有資料必須符合預設的驗證規則、外鍵限制...等。
```

**Isolation**`(隔離性)`

```
多個交易可以獨立、同時執行，不會互相干擾。
這一點跟後面會提到的「隔離層級」有關。
```
**Durability**`(持久性)`

```
交易完成後，異動結果須完整的保留。
```

<br>

### 開始交易
```sql
BEGIN TRANSACTION
START TRANSACTION
```

**SQL 測試**
```sql
insert into app_info (name, version, author, `date`, remark)
values('App', '1.0.1', 'devAuth', '2021-11-15 00:00:00', 'App-v1.0.1');

update app_info set name = 'AppNew', version='1.0.2', remark='appnew-v1.0.2' where id=12;

delete from app_info where id=9;
```

### 結束交易
```sql
COMMIT -- 提交交易，完成儲存
ROLLBACK -- 回滾交易，取消變更
```

+ DDL 指令無法 ROLLBACK

### 儲存點
`START TRANSACTION -> COMMIT / ROLLBACK`

```sql
SAVEPOINT POINT_NAME
```

**Rollback Save Point**
```sql
ROLLBACK TO SAVEPOINT POINT_NAME
```

**Release Save Point**
```sql
RELEASE SAVEPOINT POINT_NAME
```

<br>

TRUNCATE Table
------

    Truncate table app_info;

+ 清空數據表格的資料

<br>

後端開發
------
`業界慣例`

### 開 Table
```sql
create table app_info (
	id int auto_increment primary key ,
	name	nvarchar(50),
	version	nvarchar(30),
	author	nvarchar(50),
	date	datetime,
	remark	nvarchar(100) ,
  create_date datetime ,
  create_user nvarchar(30) ,
  modify_date datetime ,
  modify_user nvarchar(30)
);
```

+ primary key : 主鍵 `auto_increment`
+ create_date : 創建的時間
+ create_user : 創建的使用者
+ modify_date : 修改的時間
+ modify_user : 修改的使用者
+ foreign key : 外鍵
+ index : 索引

### SQL Injection
`資訊安全，預防 SQL 注入`

**常見的網路攻擊 : SQL 注入**

inject-sql-sample.py 
```py
import pymysql

conn = pymysql.connect(
    host='localhost', port=3306 ,user='DevAuth', password='Dev127336', database='DevDb')

cursor = conn.cursor()

row_id = 1
cursor.execute('SELECT * FROM `app_info` where id = {};'.format(row_id))

records = cursor.fetchall()
print("record type => ", type(records))
print("record[i] type => ", type(records[0]))
for r in records:
    print(r)
```

**預防 SQL 注入 : 引數化**

prevent-inject-sql.py
```sql
import pymysql

conn = pymysql.connect(
    host='localhost', port=3306, user='DevAuth', password='Dev127336', database='DevDb')

cursor = conn.cursor()

row_id = "1"
#row_id = "1 or 1=1"

values = [row_id]
cursor.execute('SELECT * FROM `app_info` where id = %s;', values)

records = cursor.fetchall()
print("record type => ", type(records))
print("record[i] type => ", type(records[0]))
for r in records:
    print(r)

cursor.close()
conn.close()
```

+ 引數化的方式，讓 mysql 通過預處理的方式避免 sql 注入的存在。

<br>

**其他預防方法**

+ 正規式檢查，過濾 SQL 語法的關鍵字
+ 使用 ORM 框架
  + `安全合格的 ORM，底層實作基本會避免 SQL Injection 的問題`


<br>

Reference
------
### TutorialsPoint - SQL
<https://www.tutorialspoint.com/sql/index.htm>




