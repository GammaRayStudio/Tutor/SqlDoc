-- app_info
drop table if exists app_info;
create table app_info (
	id int auto_increment primary key ,
	name	nvarchar(50),
	version	nvarchar(30),
	author	nvarchar(50),
	date	datetime,
	remark	nvarchar(100)
);

-- 使用者資訊
drop table if exists user_info;
create table user_info (
	id int auto_increment primary key ,
	account	    nvarchar(50) not null unique,
	password	nvarchar(50) not null ,
	name	    nvarchar(50) not null ,
	job_no	    nvarchar(50) not null unique,
	email	    nvarchar(50) not null unique,
	sys_role    int not null,
	enable	    bit not null,
    create_time datetime ,
    create_user nvarchar(50) ,
    modify_time datetime ,
    modify_user nvarchar(50)
);

-- 代理資訊
drop table if exists agent_info;
create table agent_info (
	id int auto_increment primary key ,
    user_id int unique references user_info(id)  ,
    recommender_id int references user_info(id),
    level_id     int ,
	line_id	     nvarchar(50),
	wechat_id    nvarchar(50),
	phone	     nvarchar(50),
	address	     nvarchar(50),
	address_type nvarchar(50),
	enable	     bit ,
    create_time datetime ,
    create_user nvarchar(50) ,
    modify_time datetime ,
    modify_user nvarchar(50)
);

-- 業務分支管理
drop table if exists business_branch;
create table business_branch(
	id int auto_increment primary key ,
    business_id int references user_info(id) ,
    branch_id int references user_info(id)
);

-- 代理補貼資訊
drop table if exists agent_subsidy;
create table agent_subsidy (
	id int auto_increment primary key ,
    level_id     int ,
    level_name nvarchar(50),
    agent_price int ,
    achieve_order_count int,
    achieve_subsidy int,
    promote_order_count int,
    promote_subsidy int,
    create_time datetime ,
    create_user nvarchar(50) ,
    modify_time datetime ,
    modify_user nvarchar(50)
);

-- 代理叫貨紀錄
 drop table if exists agent_order;
 create table agent_order (
     id int auto_increment primary key ,
     user_id int references user_info(id) ,
     agent_id int references agent_info(id) ,
     level_id int ,
     agent_price int ,
     order_date datetime ,
     order_num int ,
     origin_price int ,
     record_order_count int ,
     is_achieve bit ,
     achieve_feedback int ,
     is_promotion bit ,
     promotion_level int ,
     promotion_feedback int ,
     is_allow_growing_up_bonus bit,
     growing_up_bonus_rate float ,
     growing_up_bonus int ,
     achieve_discount int ,
     promote_discount int ,
     growing_up_discount int ,
     activity_discount int ,
     payment_amount int ,
     is_payment bit ,
     payment_date datetime ,
     shipping_date datetime ,
     order_status int ,
     order_enable bit ,
     order_message nvarchar(50),
     create_time datetime ,
     create_user nvarchar(50) ,
     modify_time datetime ,
     modify_user nvarchar(50)
 );

-- 叫貨口味紀錄
drop table if exists taste_order;
create table taste_order (
	id int auto_increment primary key ,
    order_id int references agent_order(id) ,
    taste_id int references taste_info(id) ,
    order_num int ,
    create_time datetime ,
    create_user nvarchar(50) ,
    modify_time datetime ,
    modify_user nvarchar(50)
);

-- 叫貨口味資訊
drop table if exists taste_info;
create table taste_info (
	id int auto_increment primary key ,
    taste_id int ,
    taste_name nvarchar(50) ,
    release_date datetime ,
    enable bit ,
    create_time datetime ,
    create_user nvarchar(50) ,
    modify_time datetime ,
    modify_user nvarchar(50)
);

-- 達標補貼剩餘額度
drop table if exists achieve_info;
create table achieve_info (
	id int auto_increment primary key ,
	agent_id int references agent_info(id) ,
	subsidy_amount int ,
	add_subsidy int ,
	sub_subsidy int ,
    create_time datetime ,
    create_user nvarchar(50) ,
    modify_time datetime ,
    modify_user nvarchar(50)
);

-- 達標補貼異動紀錄
drop table if exists achieve_record;
create table achieve_record (
	id int auto_increment primary key ,
	agent_id int references agent_info(id) ,
	order_id int references order_info(id) ,
	change_date datetime ,
	change_code nvarchar(50) ,
	change_amount int ,
	change_event nvarchar(300),
    create_time datetime ,
    create_user nvarchar(50) ,
    modify_time datetime ,
    modify_user nvarchar(50)
);

-- 晉升補貼剩餘額度
drop table if exists promote_info;
create table promote_info (
	id int auto_increment primary key ,
	agent_id int references agent_info(id) ,
	subsidy_amount int ,
	add_subsidy int ,
	sub_subsidy int ,
    create_time datetime ,
    create_user nvarchar(50) ,
    modify_time datetime ,
    modify_user nvarchar(50)
);

-- 晉升補貼異動紀錄
drop table if exists promote_record;
create table promote_record (
	id int auto_increment primary key ,
	agent_id int references agent_info(id) ,
	order_id int references order_info(id) ,
	change_date datetime ,
	change_code nvarchar(50) ,
	change_amount int ,
	change_event nvarchar(300),
    create_time datetime ,
    create_user nvarchar(50) ,
    modify_time datetime ,
    modify_user nvarchar(50)
);

-- 成長獎金剩餘額度
drop table if exists growing_up_bonus;
create table growing_up_bonus (
	id int auto_increment primary key ,
	agent_id int references agent_info(id) ,
	subsidy_amount int ,
	add_subsidy int ,
	sub_subsidy int ,
    create_time datetime ,
    create_user nvarchar(50) ,
    modify_time datetime ,
    modify_user nvarchar(50)
);

-- 成長獎金異動紀錄
drop table if exists growing_up_record;
create table growing_up_record (
	id int auto_increment primary key ,
	agent_id int references agent_info(id) ,
	order_id int references order_info(id) ,
	change_date datetime ,
	change_code nvarchar(50) ,
	change_amount int ,
	change_event nvarchar(300),
    create_time datetime ,
    create_user nvarchar(50) ,
    modify_time datetime ,
    modify_user nvarchar(50)
);

-- 郵件內容管理
drop table if exists mail_content;
create table mail_content(
	id int auto_increment primary key ,
    mail_id int ,
    title nvarchar(500) ,
    content nvarchar(5000)
);

-- 活動獎金剩餘額度 (一帶一活動獎金)
drop table if exists activity_branch_bonus;
create table activity_branch_bonus (
	id int auto_increment primary key ,
	agent_id int references agent_info(id) ,
	subsidy_amount int ,
	add_subsidy int ,
	sub_subsidy int ,
    create_time datetime ,
    create_user nvarchar(50) ,
    modify_time datetime ,
    modify_user nvarchar(50)
);

-- 成長獎金異動紀錄 (一帶一活動獎金)
drop table if exists activity_branch_record;
create table activity_branch_record (
	id int auto_increment primary key ,
	agent_id int references agent_info(id) ,
	order_id int references order_info(id) ,
	branch_id int references user_info(id) ,
	change_date datetime ,
	change_code nvarchar(50) ,
	change_amount int ,
	change_event nvarchar(300),
    create_time datetime ,
    create_user nvarchar(50) ,
    modify_time datetime ,
    modify_user nvarchar(50)
);

