-- app_info
insert into app_info(name,version,author,date,remark) values
('JavaProjSE-v1.0.3','1.0.3','Enoxs','2019-07-24' ,'Java Project Simple Example - Version 1.0.3'),
('JUnitSE','1.0.2','Enoxs','2019-08-17' ,'Java Unit Test Simple Example'),
('SpringMVC-SE','1.0.2','Enoxs','2019-07-31' ,'Java Web Application Spring MVC 框架 - Simple Example 未整合持久化框架（MyBatis）');

-- user_info
insert into user_info (account, password, name, job_no, email, sys_role, enable,
	create_time, create_user, modify_time, modify_user) values
('DevAuth', '4ae32d6b7dd79366a54a76893bc4ef7c', '開發者', 'S000', 'dev_main@gmail.com', 1, 1, now(), 'DevAuth', null , null),
('DevUser01', '4a7d1ed414474e4033ac29ccb8653d9b', '業務', 'B000', 'dev_bus00@gmail.com', 2, 1, now(), 'DevAuth', null , null),
('DevUser02', '81dc9bdb52d04dc20036dbd8313ed055', '代理', 'P00000', 'dev_agent00@gmail.com', 3, 1, now(), 'DevAuth', null , null),
('Business01', '4a7d1ed414474e4033ac29ccb8653d9b', '業務01', 'B001', 'dev_bus01@gmail.com', 2, 1, now(), 'DevAuth', null , null),
('Business02', '4a7d1ed414474e4033ac29ccb8653d9b', '業務02', 'B002', 'dev_bus02@gmail.com', 2, 1, now(), 'DevAuth', null , null),
('Business03', '4a7d1ed414474e4033ac29ccb8653d9b', '業務03', 'B003', 'dev_bus03@gmail.com', 2, 1, now(), 'DevAuth', null , null),
('Agent01', '81dc9bdb52d04dc20036dbd8313ed055', '代理A', 'P00001', 'dev_test_A@gmail.com', 3, 1, now(), 'DevAuth', null , null),
('Agent02', '81dc9bdb52d04dc20036dbd8313ed055', '代理B', 'P00002', 'dev_test_B@gmail.com', 3, 1, now(), 'DevAuth', null , null),
('Agent03', '81dc9bdb52d04dc20036dbd8313ed055', '代理C', 'P00003', 'dev_test_C@gmail.com', 3, 1, now(), 'DevAuth', null , null),
('Agent04', '81dc9bdb52d04dc20036dbd8313ed055', '代理D', 'P00004', 'dev_test_D@gmail.com', 3, 1, now(), 'DevAuth', null , null),
('Agent05', '81dc9bdb52d04dc20036dbd8313ed055', '代理E', 'P00005', 'dev_test_E@gmail.com', 3, 1, now(), 'DevAuth', null , null),
('Agent06', '81dc9bdb52d04dc20036dbd8313ed055', '代理F', 'P00006', 'dev_test_F@gmail.com', 3, 1, now(), 'DevAuth', null , null),
('Agent07', '81dc9bdb52d04dc20036dbd8313ed055', '代理G', 'P00007', 'dev_test_G@gmail.com', 3, 1, now(), 'DevAuth', null , null),
('Agent08', '81dc9bdb52d04dc20036dbd8313ed055', '代理H', 'P00008', 'dev_test_H@gmail.com', 3, 1, now(), 'DevAuth', null , null),
('Agent09', '81dc9bdb52d04dc20036dbd8313ed055', '代理I', 'P00009', 'dev_test_I@gmail.com', 3, 1, now(), 'DevAuth', null , null),
('Agent10', '81dc9bdb52d04dc20036dbd8313ed055', '代理J', 'P00010', 'dev_test_J@gmail.com', 3, 1, now(), 'DevAuth', null , null),
('Agent11', '81dc9bdb52d04dc20036dbd8313ed055', '代理K', 'P00011', 'dev_test_K@gmail.com', 3, 1, now(), 'DevAuth', null , null),
('Agent12', '81dc9bdb52d04dc20036dbd8313ed055', '代理L', 'P00012', 'dev_test_L@gmail.com', 3, 1, now(), 'DevAuth', null , null);

-- password
-- Dev127336 , 4ae32d6b7dd79366a54a76893bc4ef7c
-- 0000 , 4a7d1ed414474e4033ac29ccb8653d9b
-- 1234 , 81dc9bdb52d04dc20036dbd8313ed055

-- DevUser02 : 無
-- Agent01 : 小代理 , 已叫貨貨
-- Agent02 : 中代理 ,


-- agent_info
insert into agent_info (user_id , recommender_id, level_id, line_id, wechat_id, phone, address, address_type ,
	enable, create_time, create_user, modify_time, modify_user) values
(3, 2, 0, 'dev123', 'dev456', '0910-123456', '110台北市信義區信義路五段7號', '7-11', 0, now(), 'B000', null, null) ,
(7, 3, 0, 'dev123', 'dev456', '0910-123456', '110台北市信義區信義路五段20號', '7-11', 0, now(), 'P00000', null, null) ,
(8, 3, 0, 'dev123', 'dev456', '0910-123456', '110台北市信義區市府路45號', '7-11', 0, now(), 'P00000', null, null) ,
(9, 3, 0, 'dev123', 'dev456', '0910-123456', '106台北市大安區新生南路二段1號', '7-11', 0, now(), 'P00000', null, null) ,
(10, 4, 0, 'dev123', 'dev456', '0910-123456', '100台北市中正區中華路二段307巷', '7-11', 0, now(), 'B001', null, null) ,
(11, 10, 0, 'dev123', 'dev456', '0910-123456', '台北市中正區北平西路3號100臺灣', '7-11', 0, now(), 'P00004', null, null),
(12, 11, 0, 'dev123', 'dev456', '0910-123456', '100台北市中正區中華路二段307巷', '7-11', 0, now(), 'P00005', null, null),
(13, 5, 0, 'dev123', 'dev456', '0910-123456', '110台北市信義區信義路五段20號', '7-11', 0, now(), 'B002', null, null),
(14, 5, 0, 'dev123', 'dev456', '0910-123456', '110台北市信義區市府路45號', '7-11', 0, now(), 'B002', null, null),
(15, 5, 0, 'dev123', 'dev456', '0910-123456', '106台北市大安區新生南路二段1號', '7-11', 0, now(), 'B002', null, null),
(16, 6, 0, 'dev123', 'dev456', '0910-123456', '110台北市信義區信義路五段20號', '7-11', 0, now(), 'B003', null, null),
(17, 6, 0, 'dev123', 'dev456', '0910-123456', '100台北市中正區中華路二段307巷', '7-11', 0, now(), 'B003', null, null),
(18, 16, 0, 'dev123', 'dev456', '0910-123456', '台北市中正區北平西路3號100臺灣', '7-11', 0, now(), 'P00010', null, null);

-- 2 (B000)
--      3(P00000) ,
--          7(P00001) , 8(P00002) , 9(P00003)
-- 4 (B001)
--      10(P00004) ,
--          11(P00005)
--              12(P00006)
-- 5 (B002)
--      13(P00007) , 14(P00008) , 15(P00009)
-- 6 (B003)
--      16(P00010) , 17(P00011)
--          18(P00012)



-- bussiness_branch
insert into business_branch(business_id , branch_id) values
( 2 , 3 ),
( 2 , 7 ),
( 2 , 8 ),
( 2 , 9 ),
( 4 , 10),
( 4 , 11),
( 4 , 12),
( 5 , 13),
( 5 , 14),
( 5 , 15),
( 6 , 16),
( 6 , 17),
( 6 , 18);


-- agent_subsidy
insert into agent_subsidy (level_id , level_name, agent_price, achieve_order_count , achieve_subsidy , promote_order_count, promote_subsidy ,
create_time, create_user, modify_time, modify_user) values
( 0 , '無' , 160 , 0 , 0 , 0 , 0 , now() , 'DevAuth' , null , null ),
( 1 , '小代理' , 160 , 300 , 600 , 30 , 0 , now() , 'DevAuth' , null , null ),
( 2 , '中代理' , 140 , 0 , 0 , 500 , 1800 , now() , 'DevAuth' , null , null ),
( 3 , '大代理' , 130 , 0 , 0 , 3000 , 5000 , now() , 'DevAuth' , null , null ),
( 4 , '分銷商' , 110 , 0 , 0 , 5000 , 10000 , now() , 'DevAuth' , null , null );

-- taste_info
insert into taste_info (taste_id, taste_name, release_date, enable,
    create_time, create_user, modify_time, modify_user) values
(1, '草莓', now() , 1, now() , 'DevAuth' , null , null ),
(2, '藍莓', now(), 1, now() , 'DevAuth' , null , null ),
(3, '芒果', now(), 1, now() , 'DevAuth' , null , null ),
(4, '老冰棍', now(), 1, now() , 'DevAuth' , null , null ),
(5, '綠豆冰', now(), 1, now() , 'DevAuth' , null , null ),
(6, '葡萄', now(), 1, now() , 'DevAuth' , null , null ),
(7, '野莓', now(), 1, now() , 'DevAuth' , null , null ),
(8, '蜜瓜', now(), 1, now() , 'DevAuth' , null , null ),
(9, '可樂', now(), 1, now() , 'DevAuth' , null , null ),
(10, '百香果', now(), 1, now() , 'DevAuth' , null , null );

-- mail_content
insert into mail_content (mail_id, title , content) values
( 1 , '代理業務資訊系統 | 訂單確認通知' , '<h3>${name}，您好!</h3><p>業務窗口已收到您的訂單。</p>');

