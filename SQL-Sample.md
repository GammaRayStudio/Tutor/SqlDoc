SQL Sample
======


Login Page
------
`登入頁面`

### user_info
`使用者資訊`

| id | name | account | password | photo |
| --- | --- | --- | --- | --- |
| 1 | Gamma | DevAuth | 123456 | 1 |
| 2 | Ray | root | 000000 | 2 |
| 3 | Studio | sa | 12345678 | 3 |

**ddl**
```sql
create table user_info (
    id int auto_increment primary key ,
    name    nvarchar(50),
    account    nvarchar(30),
    password    nvarchar(20),
    photo  int
);
```

**data**
```sql
insert into user_info(id , name , account , password , photo) values
(1,'Gamma','DevAuth','123456',1),
(2,'Ray','root','000000',2),
(3,'Studio','sa','12345678',3);
```

<br>

Blogger
------
`部落格`

### article
`文章`

| id | title | content | author | date |
| --- | --- | --- | --- | --- |
| 1 | Select | Query Text | DevAuth | 2022-01-20 |
| 2 | Insert | Add Text Text | DevAuth | 2022-01-21 |
| 3 | Update | Modify Text | DevAuth | 2022-01-22 |
| 4 | Delete | Remove Text | DevAuth | 2022-01-23 |
| 5 | Where | Query Text by Id | DevAuth | 2022-01-24 |

**ddl**
```sql
create table article (
    id int auto_increment primary key ,
    title    nvarchar(200),
    content    nvarchar(1200),
    author    nvarchar(30),
    date    datetime  
);
```

**data**
```sql
insert into article(id , title , content , author , date ) values
(1,'Select','Query Text', 'DevAuth' , '2022-01-20'),
(2,'Insert','Add Text Text', 'DevAuth' , '2022-01-21' ),
(3,'Update','Modify Text', 'DevAuth' , '2022-01-22' ),
(4,'Delete','Remove Text', 'DevAuth' , '2022-01-23' ),
(5,'Where','Query Text by Id', 'DevAuth' , '2022-01-24' );
```

### message
`留言`

| id | article_id | message | author | date | 
| --- | --- | --- | --- | --- |
| 1 | 1 | Text Text Text | sa | 2022-01-20 |
| 2 | 1 | Text Text Text | root | 2022-01-21 |

**ddl**
```sql
create table message (
    id int auto_increment primary key ,
    article_id    int,
    message    nvarchar(500),
    author    nvarchar(30),
    date    datetime  
);
```

**data**
```sql
insert into message(id , article_id , message , author , date ) values
(1,1,'Text Text Text', 'sa' , '2022-01-20'),
(2,2,'Text Text Text', 'root' , '2022-01-21' );
```

### JOIN_TABLE

**article & message**
```
select * from article as a
inner join message as b
on a.id = b.id;
```

**article & message & user_info**
```sql
select * from article as a
inner join message as b
on a.id = b.id
left join user_info as c
on b.author = c.account ;
```



<br>

System
------
### update_file
`上傳檔案`

| id | name | type |
| --- | --- | --- |
| 1 | 97f4jufo | png |
| 2 | i37tu5fg | jpg |
| 3 | soyq5n1s | jpeg |

**ddl**
```sql
create table update_file (
    id int auto_increment primary key ,
    name    nvarchar(10),
    type    nvarchar(10)
);
```

**data**
```sql
insert into update_file(id , name , type ) values
(1,'97f4jufo' , 'png'),
(2,'i37tu5fg' , 'jpg'),
(3,'soyq5n1s' , 'jpeg');
```

### logging_data
`日誌記錄`

| id | action | create_user | create_date | modify_user | modify_date |
| --- | --- | --- | --- | --- | --- |
| 1 | new article | DevAuth | 2022-01-20 | null | null |
| 2 | new message | sa | 2022-01-19 | sa | 2022-01-20 |

**ddl**
```sql
create table logging_data (
    id int auto_increment primary key ,
    action    nvarchar(50),
    create_user    nvarchar(30),
    create_date    datetime,
    modify_user    nvarchar(30),
    modify_date    datetime
);
```

**data**
```sql
insert into logging_data(id , action , create_user , create_date , modify_user , modify_date ) values
(1,'new article' , 'DevAuth', '2022-01-20' , null , null),
(2,'new message' , 'sa' ,  '2022-01-19' , 'sa' , '2022-01-20');
```

### JOIN_TABLE

**user_info & update_file**
```sql
select a.account , b.name , b.type from user_info a
inner join update_file b
on a.photo = b.id;
```


**user_info & logging_data**
```sql
select a.account , b.action , b.create_date , b.modify_user , b.modify_date from user_info a
inner join logging_data b
on a.account = b.create_user;
```


