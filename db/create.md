開發測試資料表
======


Contents
------
+ app_info

<br>

app_info
------
`程式專案資訊`

| name | type | spec | desc |
| ---- | ---- | ---- | --- |
| id     | long | key && auto |
| name   | text | 50 | 專案名稱 |
| version| text | 30 | 專案版本 |
| author | text | 50 | 專案作者 | 
| date   | datetime | &nbsp; | 發佈時間 |
| remark | text | 100 | 專案描述 |


