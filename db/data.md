開發測試資料
======

app_info
------
Field
    
    id,name,version,author,date,remark
    
+ id=1
    + 'JavaProjSE-v1.0.3',
    + '1.0.3',
    + 'Enoxs',
    + '2019-07-24',
    + 'Java Project Simple Example - Version 1.0.3'
+ id=2
    + 'JUnitSE',
    + '1.0.2',
    + 'Enoxs',
    + '2019-08-17',
    + 'Java Unit Test Simple Example'
+ id=3
    + 'SpringMVC-SE',
    + '1.0.2',
    + 'Enoxs',
    + '2019-07-31',
    + 'Java Web Application Spring MVC'
    