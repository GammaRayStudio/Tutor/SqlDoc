SQL 語法速查表
======
`SQL 語法不區分大小寫`

目錄
------
+ SELECT 語法
+ CREATE TABLE 語法
+ DROP TABLE 語法
+ CREATE INDEX 語法
+ DROP INDEX 語法
+ DESC 語法
+ TRUNCATE TABLE 語法
+ ALTER TABLE 語法
+ ALTER TABLE RENAME 語法
+ INSER INTO 語法
+ UPDATE 語法
+ DELETE 語法
+ CREATE DATABASE 語法
+ DROP DATABASE 語法
+ USE 語法
+ COMMIT 語法
+ ROLLBACK 語法

<br>

SELECT 語法
------
```sql
select column01 , column02 , ... columnN
from table_name;
```

<br>

### DISTINCT 子句
```sql
select distinct column01 , column02 , ... columnN
from table_name;
```

<br>

### WHERE 子句
```sql
select column01 , column02 , ... columnN
from table_name
where condition;
```

<br>

### AND/OR 子句
```sql
select column01 , column02 , ... columnN
from table_name
where condition01 and condition02;

select column01 , column02 , ... columnN
from table_name
where condition01 or condition02;
```

<br>

### IN 子句
```sql
select column01 , column02 , ... columnN
from table_name
where column_name in (value01 , value02 , value03);
```

<br>

### BETWEEN 子句
```sql
select column01 , column02 , ... columnN
from table_name
where column_name between value01 and value02;
```

<br>

### LIKE 子句
```sql
select column01 , column02 , ... columnN
from table_name
where column_name like pattern;
```

<br>

### ORDER BY 子句
```sql
select column01 , column02 , ... columnN
from table_name
where condition 
order by column_name ase

select column01 , column02 , ... columnN
from table_name
where condition 
order by column_name desc
```

<br>

### GROUP BY 子句
```sql
select sum(column_name)
from table_name
where condition
group by column_name;
```

<br>

### COUNT 子句
```sql
select count(column_name)
from table_name
where condition;
```

<br>

### HAVING 子句
```sql
select sum(column_name)
from table_name
where condition
group by column_name
having (arithematic function condition)
```

<br>

CREATE TABLE 語法
------
```sql
create table table_name(
column01 datatype ,
column02 datatype ,
column03 datatype ,
...
columnN datatype ,
PRIMARY KEY (one or more columns)
); 
```

<br>

DROP TABLE 語法
------
```sql
drop table table_name;
```

<br>

CREATE INDEX 語法
------
```sql
create unique index index_name
on table_name ( column01 , column02 , ... columnN )
```

<br>

DROP INDEX 語法
------
```sql
alter table table_name
drop index index_name;
```

<br>

DESC 語法
------
```sql
desc table_name;
```

<br>

TRUNCATE TABLE 語法
------
```sql
truncate table table_name;
```

<br>

ALTER TABLE 語法
------
```sql
alter table table_name add column_name data_type;
alter table table_name drop column_name data_type;
alter table table_name modify column_name data_type;
```

<br>

ALTER TABLE RENAME 語法
------
```sql
alter table table_name rename to new_table_name;
```

<br>

INSER INTO 語法
------
```sql
insert into table_name ( column01 , column02 , ... columnN )
values ( value01 , value02 , ... valueN );
```

<br>

UPDATE 語法
-----
```sql
update table_name
set column01 = value01 , column02 = value02 , ... columnN
where condition;
```

DELETE 語法
------
```sql
delete from table_name
where condition;
```

CREATE DATABASE 語法
------
```sql
create database database_name;
```

DROP DATABASE 語法
------
```sql
drop database database_name;
```

USE 語法
------
```sql
use database_name;
```

COMMIT 語法
------
```sql
commit;
```

ROLLBACK 語法
------
```sql
rollback;
```











