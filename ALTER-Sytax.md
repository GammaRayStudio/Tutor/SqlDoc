ALTER 語法速查表
======


DEMO TABLE
------
### Create
```sql
create table app_info (
	id int auto_increment primary key ,
	name	nvarchar(50),
	version	nvarchar(30),
	author	nvarchar(50),
	date	datetime,
	remark	nvarchar(100)
);
```

### Data
```sql
insert into app_info(name,version,author,date,remark) values
('JavaProjSE-v1.0.3','1.0.3','Enoxs','2019-07-24' ,'Java Project Simple Example - Version 1.0.3'),
('JUnitSE','1.0.2','Enoxs','2019-08-17' ,'Java Unit Test Simple Example'),
('SpringMVC-SE','1.0.2','Enoxs','2019-07-31' ,'Java Web Application Spring MVC');
```

<br>

New Column
------
### Synax
```sql
ALTER TABLE TABLE_NAME ADD COLUMN_NAME DATATYPE;
```

### SQL
```sql
alter table app_info add new_column nvarchar(64) null;
```

<br>

Change Column
------
### Synax
```sql
ALTER TABLE TABLE_NAME CHANGE COLUMN_NAME COLUMN_NEW_NAME DATATYPE;
```

### SQL
```sql
alter table app_info change new_column description nvarchar(100) null;
```

<br>

Drop Column
------
### Synax
```sql
ALTER TABLE TABLE_NAME DROP COLUMN COLUMN_NAME;
```

### SQL
```sql
alter table app_info drop column description
```

<br>

Change Data Type
------
### Synax
```sql
ALTER TABLE TABLE_NAME MODIFY COLUMN COLUMN_NAME DATATYPE;
```

### SQL
```sql
alter table app_info modify column description varchar(300);
```

<br>

Add Not Null
------
### Synax
```sql
ALTER TABLE TABLE_NAME MODIFY COLUMN_NAME DATATYPE NOT NULL;
```

### SQL
```sql
alter table app_info modify description varchar(300) not null;
```

<br>

ADD UNIQUE CONSTRAINT
------
### Synax
```sql
ALTER TABLE TABLE_NAME ADD CONSTRAINT MYUNIQUECONSTRAINT UNIQUE(COLUMN1, COLUMN2...);
```

### SQL
```sql
alter table app_info add constraint myuniqueconstraint unique(id, name);
```


ADD CHECK CONSTRAINT
------
### Synax
```sql
ALTER TABLE TABLE_NAME ADD CONSTRAINT MYUNIQUECONSTRAINT CHECK (CONDITION);
```

### SQL
```sql
alter table app_info add constraint mycheckconstraint check ('date' > '2021-01-01 12:30:00');
```


ADD PRIMARY KEY
------
### Synax
```sql
ALTER TABLE TABLE_NAME ADD CONSTRAINT MYPRIMARYKEY PRIMARY KEY (COLUMN1, COLUMN2...);
```

### SQL
```sql
alter table app_info add constraint pk_custid primary key (id, name);
```


DROP CONSTRAINT
------
### Synax
```sql
ALTER TABLE TABLE_NAME DROP CONSTRAINT MYUNIQUECONSTRAINT;
```

### SQL
```sql
alter table app_info drop primary key;
alter table app_info drop constraint mycheckconstraint;
alter table demo_index drop index idx_name;
```

