SQL , LIKE 子句速查表
======
`Wildcard Operators , 通配符`

百分比符號 `%`
------
`匹配 1 ~ N 個字元`

```sql
select * from app_info where name like '%SE';

select * from app_info where name like '%SE-%';

select * from app_info where name like 'J%';

select * from app_info where name like 'J%E';
```

<br>

下劃線符號 `_`
------
`匹配 1 個字元`

```sql
select * from app_info where version like '1.0._';

select * from app_info where version like '_.0.2';

select * from app_info where version like '1___2';

```

<br>

混合使用
-----
```sql
-- 數字 1 開頭，至少 3 字元長度
select * from app_info where version like '1_%_%';

-- 至少兩字元，結尾隨意
select * from app_info where version like '_._.%';

```





